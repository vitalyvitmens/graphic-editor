# Графический редактор:
https://graphic-editor-c1e89.web.app/

## Стек технологий
### Frontend:
* Vite
* React
* Redux (Redux Toolkit)
* TypeScript
* CSS
* Дизайн-система Mantine UI

### Backend:
* Google Firebase (Realtime Database, Storage)

### Тестирование:
* Jest
* Testing Library

## Запуск приложения 
```js
npm run dev
```
## Запуск тестов
```js
npm run test
```

# Структура проекта:
```
+---public
|       vite.svg
|
+---spec
|   +---integration
|   |       App.spec.tsx
|   |       countLayerLabel.spec.ts
|   |       EditableText.spec.tsx
|   |       History.spec.tsx
|   |       HistorySlice.spec.tsx
|   |       Layers.spec.tsx
|   |       LayersSlice.spec.tsx
|   |       MainPage.spec.tsx
|   |       UndoRedo.spec.tsx
|   |       Zoom.spec.tsx
|   |       ZoomSlice.spec.tsx
|   |
|   +---utils
|   |       JestStoreProvider.tsx
|   |
|   \---__mocks__
|           fileMock.ts
|           styleMock.ts
|
\---src
    |   index.css
    |   index.tsx
    |   vite-env.d.ts
    |
    +---app
    |   |   index.css
    |   |   index.tsx
    |   |
    |   +---context
    |   |       ActiveCardContext.tsx
    |   |       index.ts
    |   |       interface.ts
    |   |
    |   +---firebase
    |   |       index.ts
    |   |
    |   +---providers
    |   |       index.ts
    |   |       withRouter.tsx
    |   |
    |   \---store
    |           hooks.ts
    |           rootReducer.tsx
    |           store.tsx
    |
    +---assets
    |       react.svg
    |
    +---entities
    |   +---ActionItem
    |   |   |   index.ts
    |   |   |
    |   |   +---interface
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           index.tsx
    |   |
    |   +---ContextMenu
    |   |   |   index.ts
    |   |   |
    |   |   +---interfaces
    |   |   |       contextMenuType.ts
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           DeleteItemMenuComponent.tsx
    |   |           DownloadItemMenuComponent.tsx
    |   |           OpenItemMenuComponent.tsx
    |   |           RenameItemMenuComponent.tsx
    |   |
    |   +---LayersItem
    |   |   |   index.ts
    |   |   |
    |   |   +---interface
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           index.tsx
    |   |
    |   +---ProjectCard
    |   |   |   index.ts
    |   |   |
    |   |   +---interfaces
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           ProjectCardComponent.tsx
    |   |           RenameProjectModal.tsx
    |   |
    |   +---UndoRedo
    |   |   |   index.ts
    |   |   |
    |   |   \---ui
    |   |           index.tsx
    |   |
    |   \---Zoom
    |       |   index.ts
    |       |
    |       +---model
    |       |       slice.ts
    |       |       types.ts
    |       |
    |       \---ui
    |               index.css
    |               index.tsx
    |
    +---features
    |   |   index.ts
    |   |
    |   +---CreateProjectModal
    |   |   |   index.ts
    |   |   |
    |   |   \---ui
    |   |           CreateProjectModal.tsx
    |   |
    |   +---DeleteProjectModal
    |   |   |   index.ts
    |   |   |
    |   |   \---ui
    |   |           DeleteProjectModal.tsx
    |   |
    |   +---History
    |   |   |   index.tsx
    |   |   |
    |   |   +---model
    |   |   |       slice.ts
    |   |   |       types.ts
    |   |   |
    |   |   \---ui
    |   |           index.tsx
    |   |
    |   +---Layers
    |   |   |   index.ts
    |   |   |
    |   |   +---lib
    |   |   |       index.ts
    |   |   |
    |   |   +---model
    |   |   |       layersThunk.ts
    |   |   |       slice.ts
    |   |   |       types.ts
    |   |   |
    |   |   \---ui
    |   |           index.css
    |   |           index.tsx
    |   |
    |   \---ProjectCard
    |       \---ui
    |               ContextMenuFeature.tsx
    |
    +---pages
    |   |   index.tsx
    |   |
    |   +---main
    |   |       index.tsx
    |   |
    |   \---projects
    |           index.tsx
    |
    +---shared
    |   +---Button
    |   |   +---interfaces
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           index.css
    |   |           index.tsx
    |   |
    |   +---EditableText
    |   |   |   index.ts
    |   |   |
    |   |   \---ui
    |   |           index.css
    |   |           index.tsx
    |   |
    |   +---EditProjectForm
    |   |   |   index.tsx
    |   |   |
    |   |   +---interfaces
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           index.tsx
    |   |
    |   +---enums
    |   |       index.ts
    |   |
    |   +---helpers
    |   |       index.ts
    |   |
    |   +---hooks
    |   |       index.ts
    |   |       useFirebaseDB.ts
    |   |       useFirebaseStorage.ts
    |   |
    |   +---IconButton
    |   |   +---interfaces
    |   |   |       index.ts
    |   |   |
    |   |   \---ui
    |   |           index.css
    |   |           index.tsx
    |   |
    |   +---SliderHover
    |   |   \---ui
    |   |           index.tsx
    |   |
    |   \---ui
    |       |   ContextMenuItemComponent.tsx
    |       |   index.ts
    |       |
    |       +---Loader
    |       |   \---ui
    |       |           Loader.tsx
    |       |
    |       +---NewProjectForm
    |       |   |   index.tsx
    |       |   |
    |       |   +---interfaces
    |       |   |       index.ts
    |       |   |
    |       |   \---ui
    |       |           index.tsx
    |       |
    |       \---Notification
    |           \---ui
    |                   Notification.tsx
    |                   style.css
    |
    \---widgets
        |   index.ts
        |
        +---BottomBar
        |   |   index.ts
        |   |
        |   \---ui
        |           index.css
        |           index.tsx
        |
        +---Canvas
        |   +---interfaces
        |   |       index.tsx
        |   |
        |   +---model
        |   |       slice.ts
        |   |
        |   +---ui
        |   |       index.css
        |   |       index.tsx
        |   |
        |   \---utils
        |           index.tsx
        |
        +---Header
        |   \---ui
        |           index.css
        |           index.tsx
        |
        +---ProjectCard
        |       ProjectCardWidget.tsx
        |
        +---ProjectCardList
        |   +---model
        |   |       slice.ts
        |   |       types.ts
        |   |
        |   \---ui
        |           index.css
        |           index.tsx
        |
        +---ProjectCardListEmpty
        |       ProjectCardListEmpty.tsx
        |
        +---Sidebar
        |   |   index.ts
        |   |
        |   \---ui
        |           index.css
        |           index.tsx
        |
        \---Toolbar
            +---interfaces
            |       index.ts
            |
            +---model
            |       slice.ts
            |
            \---ui
                    index.css
                    index.tsx
```